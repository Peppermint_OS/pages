===================
Welcome to PepDocs!
===================
**PeppermintOS** is a Linux distro based on Debian/DeVuan
This documentation can help you get better acquainted with the operating system
online support can be found `here <https://sourceforge.net/p/peppermintos/pepos/>`_

---------------------
This is Peppermint OS
---------------------

CI / CD
=======
This is the “pipeline” that we use for the Development team.

CI or Continuous Integration is the practice of automating the integration of
code changes from multiple developers into a single code-base. It is a software
development practice where the developers commit their work frequently into the
central code repository (Codeberg). Then there are automated tools that build
the newly committed code and do a code review, etc as required upon integration.

The tools we use can be found `here <http://peppermintos.org/>`_


**Why CI?**

There have been scenarios when Peppermint developers, worked in isolation for an
extended period of time and only merge their changes to the main branch once
their work was completed. This not only makes the merging of code very difficult
, prone to conflicts, and time-consuming but also results in bugs accumulating
for a long time which are only identified in later stages of development.
These factors make it harder to deliver updates to the system quickly.

With Continuous Integration, the peppermint developers can frequently commit to
a shared common repository using CodeBerg. A continuous integration pipeline can
automatically run builds, store the artifacts, run unit tests and even conduct
code reviews using different tools. We can configure the CI pipeline to be
triggered every time there is a commit/merge in the code-base.

**Where are we going?**

- Peppermint is morphing into its own continuous delivery OS based on the
  Debian/Devuan stable repos….with the option to pull from testing as needed.
  The audience that we want to reach are both new users as well as experienced
  Linux users.
- We want to try to meet that middle ground where it is easy enough to configure
  after install to fit new user’s needs but also not be “bloated” for the more
  experienced users, so that it can be configured with, little to no package
  removal
- Further more the option to be non-systemd can help those users who prefer sysv
  init etc....

This is why it is important for the Peppermint shift to a CI/CD pipeline using
DevOps culture, as it allows for innovation and the time needed to build
features and improvements, by our team of developers

Peppermint OS Versioning
========================
**What build of Peppermint OS are you running**

Starting with the release of Peppermint OS in Feb 2022 there is no longer any
version number or build date associated with the file names of the ISO's
published.
In the Mirrors we only host the most recent ISO builds all older builds are
replaced automatically through our pipeline.
For example:

**Debian based**

- PeppermintOS-amd64.iso - This is the most recent Debian 64 bit ISO
- PeppermintOS-i386.iso - This is the most recent Debian 32 bit ISO

**Devuan based**

- PeppermintOS-Devuan_64.iso - This is the most recent Devuan 64 bit ISO
- PeppermintOS-Devuan_32.iso - This is the most recent Devuan 32 bit ISO

If you want to know the changes that have taken place for each iteration of the
build you can review the: **BuildDate**
that is maintained
`here: <https://sourceforge.net/p/peppermintos/pepwiki/BuildDate/>`_

Yes we are based on Debian (11) and Devuan (4) and any future versions they come
out with. Any updates and point releases they do will make its way into
PeppermintOS automatically, if you want to see where you stand there you can run
in terminal
**lsb_release -a**

You will see something like this

- **Distributor ID: Peppermint** - this is us
- **Description: Peppermint OS** - this is us
- **Release: 11.4** - this is the Debian/Devuan Version
- **Codename: bullseye** - this is the name

So.... according to the above that example is running PeppermintOS Based on
Debian 11.4 Bullseye in this example. Essentially we constantly update as
needed...... we log the changes and we base on Debian and Devuan.

-------------------------
Contribute to the project
-------------------------
There are many ways you can contribute to Peppermint, you do not need to
necessarily be a developer.

#. You can support other Pep's on the forum

#. You can contribute to documentation

#. You can submit your wallpapers

#. You can always provide feedback

#. You can submit your ideas

#. You can provied translations

*With the Peppermint Community, there are so many innovations we can build
together.*


How to Contribute to PepDocs
============================
This documentation uses **sphinx**.
Sphinx is a tool that makes it easy to create intelligent and beautiful
documentation, written by Georg Brandl and licensed under the BSD license.

All you need to get started is install sphinx to your computer you can learn
how to do that using this tutorial `here <https://sphinx-tutorial.readthedocs.io/start/>`_  this tutorial also
teaches you how to build your updates via sphinx.

After you get that part up and running you can then contact the Peppermint team
via the forum to get started on committing to the PepDocs repository out on codeberg.org.

**sphinx** uses reStructuredText (reST) you can read more about that
`here <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_

------------------------------
Tools used and custom features
------------------------------
**ISO Builder:**

- The way the ISO is created is by using the **live-build** tools provided by
  Debian. If you are interested in learning about live-build you can click `here <https://live-team.pages.debian.net/live-manual/html/live-manual/index.en.html>`_
  to read the live-build manual.
- We have chosen to be very modular so that we can make major or minor changes
  to the ISO as we build new features or fix issues that the community has found.
- You can view our entire builder code out on codeberg.org by clicking `here <https://codeberg.org/Peppermint_OS/bubbles>`_ .


Welcome Screen
=====================

The welcome screen is there to help get you started with your system.

- About will open a window that loads the about page for Peppermint OS
- Suggested allows you to pick any of the web browsers that are in the stable
  repositories of Debian.
- Pepdocs will open the Peppermint Documentation
- Build Log will open the log that you can read to see changes
- Community these open in SSBs to their perspective, websites.

hBlock
======
**What is this for?**

hBlock is a POSIX-compliant shell script that gets a list of domains that serve
ads, tracking scripts and malware from multiple sources and creates a hosts
file, among other formats, that prevents your system from connecting to them.

warning:
    **hBlock by default replaces the hosts file of your system consider
    making a backup first if you have entries you want to preserve**

The default behavior of hBlock can be adjusted with multiple options.
Use the --help option to see what is available

- To enable hBlock you can open a terminal type

.. code:: shell

    hblock

- To disable hBlock you can open a terminal type

.. code:: shell

    hblock -S none -D none

Kumo
====
**What is Kumo?**

Kumo is a tool that is used to manage site specific web browsing(SSB).

It uses **SQLite** to store the links that are used for the SSB.

**Tkinter** is used for the interface, this means you can run Kumo independent
to whatever your desktop environment is.

Depening on your build Kumo will be using the following

 - **Lua** is used if running the 32 bit Flagship ISO
 - **LibreWolk** is used if running the 64 bit Flashship ISO
 - **FireFox** is used if running the loaded 64/32 ISO

**Features**

Kumo allows the user to create a new SSB and save it in the database.
From there the user can use Kumo to launch any specific SSB stored in the
database. Additionally once the SSB is created the user can delete the SSB as
needed. Finally Applications can be created and stored on your stored on your menus. 
Becuase all linkes are stored in SQLite moving your data from one computer to nother can 
be done easily as needed

xDaily (xD)
===========
Simply put, it is a semi automatic command line system updater that will also
upgrade the system, by opening a terminal and type in sudo xDaily enter your
password when prompted, it will clean out your cache, trim a SSD, update your
icon cache and auto-remove old repos, you will be prompted by the Y/N option
during the process. If there is a newer kernel version remember to reboot your
computer.

You can see this Wiki for more details:

https://sourceforge.net/p/peppermintos/pepwiki/xDaily%20-%20A%20Complete%20System%20Updater/

The reason for the xDaily tool is so that a user can decide when they want to
perform maint on thier system, it performs not only just updates but other
system maint commands in terminal. xDaily is a tool that is generally ran manually

xDaily will update both PepTools and System Updates

xDaily GUI 
===========
The GUI does all the same things as xDaily but without the need to use
the terminal. 

.. image:: gui.png

Each section of xDaily GUI has a short explanation of what the features
do.  


Suggested Packages
==================
This tool can be found in the Peppermint Hub or if you Open the Welcome screen
it will be the **Suggested** button.

This is NOT as custom package manager to replace any of the software stores or
the synaptic manager. It simply is that…….. a list of packages that the
Peppermint Community voted on that they felt would be useful for a starting
point for a user. That is the **Software** section  you see the packages listed

On the **Browser** side lists are all the browsers that are available in the
Debian/Devuan repos. The way that was decided was based on this wiki post from
Debian:

- https://wiki.debian.org/WebBrowsers

Anything outside of that posting will not be included.  Additionally, we steered
away from the terminal  browsers, as it was voted to do so.  Its up to the
user to determine what they want to install and how they install their browser.


Install and use nala
====================

Many of the terminal based tools in PeppermintOS has begun to use nala.
If you already have a build in-place you can install nala by running this command.

**Note**: Before you try to install nala make sure you have the PeppermintOS
repos added to your  sources list.

To check that follow these steps:

#. In your file manager browse to */etc/apt/sources.list.d/*

> If you see a file named *peppermint.list* you can successfull install nala

> If you DONOT see a file named *peppermint.list* then go look at the **Add the
PeppermintOS sources list** section first


Once those things have been verified then you can run this command

.. code-block::

   sudo apt update && sudo apt install nala

--------
How to's
--------
Add the PeppermintOS sources list
=================================
As mentioned in previous areas of this documentation all of our repo data is
maintained at CodeBerg.
With the PeppermintOS sources we are able to provde things that are not in the
sual Debian / Devuan repos, whether it is stable or testing there are times where it can be useful to distribute packages that we create or are third party and we package them.

**To add the PeppermintOS source lists follow these steps:**

1. Make a directory for the 2 .deb packages needed any where you prefer on your system
2. In terminal **cd** to the new directory
3. Run these commands to download the needed files

.. code-block::

   wget http://repo.peppermintos.com/packages/pool/main/p/pepsources/pepsources_1.0-1peppermint2_all.deb

.. code-block::

   wget http://repo.peppermintos.com/packages/pool/main/p/peppermint-keyring/peppermint-keyring_1.0-1peppermint1_all.deb

4. Then to install the source and update run these commands

.. code-block::

   sudo dpkg -i *.deb

.. code-block::

   sudo apt update

**Notes**

The pepsources_1.0-1peppermint2_all.deb package has a peppermint.list file with
2 lines pointing to repo.peppermintos.com locations.
The peppermint-keyring_1.0-1peppermint1_all.deb (keyring) package has the files
needed to verify the authenticity of the repo and the packages.

How to use the TTK Creator Theme editor
=======================================

The way that PeppermintOS adds themes to our tools is,  with **ttkboostrap**
this is a project that is maintained
by `israel-dryer <https://github.com/israel-dryer/ttkbootstrap/>`_  it is a
theme extension for tkinter that enables on-demand modern flat style themes
inspired by Bootstrap. We use this to centralize our tkinter styling.

The **TTK Creator** is a tool that is shipped with ttkbootstrap that allows you
to change the styles on the fly, that will provide a way for you to make the
PepTools better match your overall build theme.

To use TTK Creator open the Peppermint Hub and click  on the
**Hardware & Software** tab you will see the button for **TTK Creator**

When you open TTK Creatore you see a screen that looks like below.

.. image:: ttkc.png
   :scale: 60 %
   :alt: Desktop Environment Settings


Before you get started making changes….Please NOTE: Peppermint is using the
default theme named **darkly**. You will want to backup that theme prior to
making any changes.

#. To do that in the **base theme** drop-down select **darkly**
#. In the **name** field type **darkly**
#. Click the **Save** menu option
#. If you see the **Overwrite existing theme darkly?** click OK
#. Then click **Export**
#. Save that file somewhere on your computer..this is so you can reset the
   theme to the peppermint default theme any time.


Making tkinter Theme Changes
=============================
On the left side of the TTK Creator window you see all of  the html color codes
and the controls they affect that can be changed.  As you make changes to the
color codes you will see the affects on the demo side of the Creator window.

When you are done making all your preferred changes.

#. You will want to make sure you set the name to **darkly**
#. Click **Save**
#. If you see the **Overwrite existing theme darkly?** click OK


To reset the theme to the default Peppermint theme.

#. In the TTK Creator window click Import, select your file and click Open
#. At this point your file has been imported.
#. If you close and reopen TTK Creator and select **darkly** in the base theme
   you will see the default themes scheme.
#. Type darkly in the **name** field and click **Save**
#. If you see the **Overwrite existing theme darkly?** click OK

Your theme has been reset.
